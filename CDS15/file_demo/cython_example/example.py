import cv2
import os

path = 'C:/Users/Admin/Desktop/Key C PT1'

list_imgs = os.listdir(path)

def show(src):
    window = 'image'
    cv2.namedWindow(window, cv2.WINDOW_AUTOSIZE)
    cv2.imshow(window, src)
    cv2.waitKey()

    cv2.destroyAllWindows()
def run():
	n_imgs = len(list_imgs)

	for i in range(n_imgs):
		l = list_imgs[i]
		img = cv2.imread(path+'/'+l, cv2.IMREAD_COLOR)
		show(img)
		if(cv2.waitKey(30) & 0xff == 27):
			break
		if(i == n_imgs - 1):
			i = 0
run()