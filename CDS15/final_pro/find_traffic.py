import os
import numpy as np
import cv2
from prediction import Classifier
import time
kernel = np.ones((5,5), np.uint8)

low_red = (80, 70, 50)
high_red =  (100, 255, 255)
low_blue =   (100,150,0)
high_blue =  (140,255,255)

def nothing(x):
  pass

def show(img):
  window = 'window'
  cv2.namedWindow(window, cv2.WINDOW_AUTOSIZE)
  cv2.imshow(window, img)
  cv2.waitKey()
  cv2.destroyAllWindows()

def take_path(path):
  paths = []
  list_imgs = os.listdir(path)
  for img in list_imgs:
    split_name = img.split('.')
    extension = split_name[len(split_name) - 1]
    if(extension == 'jpg'):
      paths.append(img)
  return paths

def find_test():
  classifier = Classifier()
  dirFile = 'test_images/'

  paths = take_path(dirFile)
  print(paths)

  for path in paths:
    img = cv2.imread(dirFile+path, cv2.IMREAD_COLOR)
    # ret, blue = find_traffic(img, low_blue, high_blue, 'blue')
    classifier.run([img])

def find():
  classifier = Classifier()

  window = 'video'
  cv2.namedWindow(window, cv2.WINDOW_AUTOSIZE)
  video = cv2.VideoCapture('Gs8000l Test 720p 30 fps.mp4')

  n_frames = 0
  while(1):
    rat, frame = video.read()
    if(rat):
      frame = cv2.resize(frame, (480, 320))
      t = time.time()
      ret, blue = find_traffic(frame, low_blue, high_blue, 'blue')
      print(n_frames ,time.time() - t)
      if(ret):
        cv2.imshow(window, blue)
      else:
        cv2.imshow(window, frame)
      n_frames += 1
      k = cv2.waitKey(15) & 0xff
      if(k == 32):
        cv2.waitKey()
      else:
        if(k == 27):
          break
    else:
      break
  video.release()
  cv2.destroyAllWindows()

find_test()
