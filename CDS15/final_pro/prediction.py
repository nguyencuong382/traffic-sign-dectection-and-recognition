import os
import sys
dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(dir_path)


import tensorflow as tf
import numpy as np
import cv2
import modify_data

os.environ['TF_CPP_MIN_LOG_LEVEL']='2'


def show(src):
	window = 'window'
	cv2.namedWindow(window, cv2.WINDOW_AUTOSIZE)
	cv2.imshow(window, src)
	cv2.waitKey()
	cv2.destroyAllWindows()
	
class Classifier:
	def __init__(self, img_size = 32, n_classes = 16):
		self.sess = tf.Session()
		self.model = tf.train.import_meta_graph(dir_path+'/mo2/model2/traffic_sign.meta')
		self.model.restore(self.sess, tf.train.latest_checkpoint(dir_path+'/mo2/model2/./'))
		self.graph = tf.get_default_graph()
		self.x = self.graph.get_tensor_by_name('x:0')
		self.y = self.graph.get_tensor_by_name('y:0')
		self.drop = self.graph.get_tensor_by_name('drop:0')
		self.prediction = self.graph.get_tensor_by_name('model:0')

		self.predict_value = tf.reduce_max(self.prediction, axis = 1)
		self.correct = tf.argmax(self.prediction,1)

		self._n_imgs = 0
		self._img_size = img_size
		self._n_classes = n_classes
		self.label_test = np.zeros((1,self._n_classes), np.float32)
		self.name_traffic_sign = {
			1:'stop',
			2:'re trai',
			3:'re phai',
			4:'cam re trai',
			5:'cam re phai',
			6:'cam di nguoc chieu',
			7:'toc do toi da',
			8:'re phai 2',
			9:'re trai 2',
			10:'to do toi da 30',
			11:'het toc do toi da 30',
			12:'x',
			13:'x',
			14:'x',
			15:'x',
			16:'x'
		}
	@property
	def num_imgs(self):
		return self._n_imgs
	@property
	def img_size(self):
		return self._img_size
	@property
	def nun_classes(self):
		return self._n_classes

	def process_result(self, result):
		names = []
		for r in result:
			id_name = r+1
			names.append(self.name_traffic_sign[id_name])
		return names

	def run(self, imgs):
		self._n_imgs = len(imgs)

		for i in range(self._n_imgs):
			imgs[i] = modify_data.modify_img(imgs[i], self._img_size)

		re, predict_value = self.sess.run([self.correct, self.predict_value], feed_dict = {self.x:imgs, self.y:self.label_test, self.drop:1.0})
		names = self.process_result(re)

		return names, predict_value/10e8
