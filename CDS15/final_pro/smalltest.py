def union(a,b):
	x1 = min(a[0], b[0])
	y1 = min(a[1], b[1])
	x2 = max(a[2], b[2])
	y2 = max(a[3], b[3])
	return (x1, y1, x2, y2)

def intersection(a,b, area_a, area_b):
	x1 = max(a[0], b[0])
	y1 = max(a[1], b[1])
	x2 = min(a[2], b[2])
	y2 = min(a[3], b[3])
	w = x2 - x1
	h = y2 - y1
	if(w > 0 and h >0):
		min_area = min(area_a, area_b)
		if(w*h/min_area > 0.4):
			if(min_area == area_a):
				return True, b, 0
			else:
				return True, a, 0
	return False, a, b
	

def find(boxes, score):
	areas = []
	boxes_valid = []

	for box, score in zip(boxes, score):
		if(score > 1):
			area = (box[2] - box[0])*(box[3] - box[1])
			if(area > 1):
				areas.append(area)
				boxes_valid.append(box)
	   
	# print(boxes_valid)
	# print(areas)
	num_valid_boxes = len(boxes_valid)

	if(num_valid_boxes > 0):
		if(num_valid_boxes ==1):
			return boxes_valid[0]
		else:
			new_boxes = []
			for i in range(num_valid_boxes):
				for j in range(i+1, num_valid_boxes):

					rat, a, b = intersection(boxes_valid[i], boxes_valid[j], areas[i], areas[j])
					if not a in new_boxes:
						new_boxes.append(a)
					if not rat and not b in new_boxes:
						new_boxes.append(b)
			return new_boxes


boxes = [ [1,1,4,4], [3,3,5,5], [0,0,2,3] , [4,3,5,4], [4,4,5,5]]
score = [ 2,3,4,4,4]
print(find(boxes, score))

