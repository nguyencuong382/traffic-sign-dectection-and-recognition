import os
import numpy as np
import cv2
kernel = np.ones((5,5), np.uint8)

low_red = (80, 70, 50)
high_red =  (100, 255, 255)
low_blue =   (100,150,0)
high_blue =  (140,255,255)

def nothing(x):
  pass

def show(img):
  window = 'window'
  cv2.namedWindow(window, cv2.WINDOW_AUTOSIZE)
  cv2.imshow(window, img)
  cv2.waitKey()
  cv2.destroyAllWindows()

def find_traffic(img, low, high, color):
  width, height, _ = img.shape
  if(color == 'red'):
         img2 = img
         img = ~img
  else: img2 = img

  hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
  hsv = cv2.medianBlur(hsv, 9)
  mask = cv2.inRange(hsv, low, high)
  mask = cv2.medianBlur(mask, 3)
  mask = cv2.erode(mask, kernel, iterations=1)
  mask = cv2.dilate(mask, kernel, iterations=1)
  mask = cv2.erode(mask, kernel, iterations=1)
  mask = cv2.dilate(mask, kernel, iterations=1)
  im2, contours, hierarchy = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
  
  if (len(contours) > 0):
    areaArray = []
    for c in contours:
            epsilon = 0.06 * cv2.arcLength(c, True)
            approx = cv2.approxPolyDP(c, epsilon, True)
            areaArray.append(approx)
    max_contour = max(areaArray , key=cv2.contourArea)
    x, y, w, h = cv2.boundingRect(max_contour)
    print(x,y,w,h)
    if(w*h > 5000 and x > width/2):
      h = y+h
      w = x+w
      traffic = img2[y:h, x:w]
      return True, traffic
      # cv2.rectangle(img3, (x,y), (w, h), 255, 2)
  return False, 0
def take_path(path):
  paths = []
  list_imgs = os.listdir(path)
  for img in list_imgs:
    split_name = img.split('.')
    extension = split_name[len(split_name) - 1]
    if(extension == 'jpg'):
      paths.append(img)
  return paths

def find():
  paths = take_path('input/')
  print(paths)

  n_imgs = 0;
  for path in paths:
    print(path)
    img = cv2.imread('input/'+path, cv2.IMREAD_COLOR)
    # ret, blue = find_traffic(img, low_blue, high_blue, 'blue')
    ret, red = find_traffic(img, low_red, high_red, 'red')
    if(ret):
      cv2.imwrite('../data/{}.jpg'.format(n_imgs), red)
      n_imgs+=1

find()
