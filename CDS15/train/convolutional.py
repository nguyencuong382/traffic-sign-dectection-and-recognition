import tensorflow as tf
import os
import numpy as np
import cv2
import pickle
import modify_data
from modify_data import DataSet
import shutil

os.environ['TF_CPP_MIN_LOG_LEVEL']='2'

path = '../data'
n_classes = 16
batch_size = 32
keep_prob = 0.75
size_img = 32
size_wconv = [32,64,128]
size_wfc = [128*4*4]
size_fc = [1024, 512]
size_kernel = 5


def data():
	with open('data_train.p', 'rb') as f:
		data_train_sets = pickle.load(f)
	with open('data_test.p', 'rb') as f:
		data_test_sets = pickle.load(f)
	return data_train_sets, data_test_sets

#height x width
x = tf.placeholder('float', [None, size_img,size_img,3], name='x')
y = tf.placeholder('float', [None, n_classes], name = 'y')
drop = tf.placeholder(tf.float32, name='drop')

def conv2d(x, W):
	return tf.nn.conv2d(x, W, strides=[1,1,1,1], padding='SAME')
def maxpool2d(x):
	return tf.nn.max_pool(x, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME')

def convolutional_neural_network(x):
	weights = {'W_conv1': tf.Variable(tf.random_normal([size_kernel,size_kernel,3,size_wconv[0]])),
				'W_conv2': tf.Variable(tf.random_normal([size_kernel,size_kernel,size_wconv[0],size_wconv[1]])),
				'W_conv3': tf.Variable(tf.random_normal([size_kernel,size_kernel,size_wconv[1],size_wconv[2]])),
				'W_fc': tf.Variable(tf.random_normal([size_wfc[0],size_fc[0]])),
				'W_fc1': tf.Variable(tf.random_normal([size_fc[0],size_fc[1]])),
				'out': tf.Variable(tf.random_normal([size_fc[1],n_classes]))}
	biases = {'b_conv1': tf.Variable(tf.random_normal([size_wconv[0]])),
				'b_conv2': tf.Variable(tf.random_normal([size_wconv[1]])),
				'b_conv3': tf.Variable(tf.random_normal([size_wconv[2]])),
				'b_fc': tf.Variable(tf.random_normal([size_fc[0]])),
				'b_fc1': tf.Variable(tf.random_normal([size_fc[1]])),
				'out': tf.Variable(tf.random_normal([n_classes]))}

	#x = tf.reshape(x,shape=[-1,128,128,3])

	conv1 = conv2d(x,weights['W_conv1']) + biases['b_conv1']
	conv1 = maxpool2d(conv1)

	conv2 = conv2d(conv1, weights['W_conv2'])+ biases['b_conv2']
	conv2 = maxpool2d(conv2)
	
	conv3 = conv2d(conv2, weights['W_conv3'])+ biases['b_conv3']
	conv3 = maxpool2d(conv3)
	
	fc = tf.reshape(conv3, shape = [-1, size_wfc[0]])
	fc = tf.nn.relu(tf.matmul(fc,weights['W_fc'])+biases['b_fc'])
	fc = tf.nn.dropout(fc, drop)

	fc1 = tf.nn.relu(tf.matmul(fc,weights['W_fc1'])+biases['b_fc1'])
	fc1 = tf.nn.dropout(fc1, drop)

	output = tf.add(tf.matmul(fc1, weights['out']), biases['out'], name = 'model')

	return output
def save(min_loss, max_pre, start_epoch):
	dirFile = './logs/saver.txt'
	with open(dirFile, 'w') as f:
		f.write(str(min_loss)+'\n')
		f.write(str(max_pre)+'\n')
		f.write(str(start_epoch))
def restore():
	dirFile = './logs/saver.txt'
	with open(dirFile, 'r') as f:
		data = f.read()
		data = data.split('\n')
		return float(data[0]), float(data[1]), int(data[2])
def take_graph_restore(dirModel):
	tf.reset_default_graph()
	imported_meta = tf.train.import_meta_graph(os.path.join(dirModel, 'traffic_sign.meta'))
	graph = tf.get_default_graph()
	x = graph.get_tensor_by_name('x:0')
	y = graph.get_tensor_by_name('y:0')
	drop = graph.get_tensor_by_name('drop:0')

	prediction = graph.get_tensor_by_name('model:0')

	return x, y, drop, prediction

def train_neural_network(x, y, drop, hm_epochs = 100, isContinue = False):
	print('ready to train........')
	prediction = None
	optimizer  = None
	accurancy = None
	dirLog = './logs/nn_logs'
	dirModel = './model'
	if isContinue:
		x, y, drop, prediction = take_graph_restore(dirModel)
	else:
		prediction = convolutional_neural_network(x)
		with tf.name_scope('cost'):
			cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits = prediction, labels = y))
			optimizer = tf.train.AdamOptimizer(0.0005).minimize(cost, name = 'optimizer')
			tf.add_to_collection('cost', cost)
			tf.add_to_collection('optimizer', optimizer)
			tf.summary.scalar('cost', cost)

		with tf.name_scope("accurancy"):
			correct = tf.equal(tf.argmax(prediction, 1), tf.argmax(y,1))
			accurancy = tf.reduce_mean(tf.cast(correct,'float'), name = 'accurancy')
			tf.add_to_collection('acc', accurancy)
			tf.summary.scalar("accurancy", accurancy)
	#------------------------------------------#

	
	
	saver = tf.train.Saver()
		
	with tf.Session() as sess:
		sess.run(tf.global_variables_initializer())
		min_loss = 0.0
		max_pre = 0.0
		start_epoch = 0

		if isContinue:
			# model = tf.train.import_meta_graph('model/traffic_sign.meta')
			saver.restore(sess, 'model/traffic_sign')
			cost = tf.get_collection('cost')[0]
			optimizer = tf.get_collection('optimizer')[0]
			accurancy = tf.get_collection('acc')[0]
			tf.summary.scalar('cost', cost)
			tf.summary.scalar('cost', cost)

			min_loss, max_pre, start_epoch = restore()
			print('Previous min loss: ', min_loss)
			print('Previous min max pre: ', max_pre)
			print('Previous min start epoch: ', start_epoch)

		else:
			if os.path.exists(dirLog):
				shutil.rmtree(dirLog)


		writer = tf.summary.FileWriter('./logs/nn_logs', sess.graph)
		merged = tf.summary.merge_all()

		
		data_train, data_test = data()
		print('loaded data....')

		n_data = data_train.num_examples			
		im_test, label_test = data_test.next_batch(data_test.num_examples)

		for epoch in range(start_epoch,hm_epochs):

			epcho_loss = 0

			for i in range(int(n_data/batch_size)+1):
				epcho_x, epcho_y = data_train.next_batch(batch_size)
				_, c = sess.run([optimizer, cost], feed_dict = {x:epcho_x, y:epcho_y, drop:keep_prob})
				print(i, c)
				epcho_loss += c
			
			print('Epoch ', epoch, 'complete out of ', hm_epochs, 'loss' , epcho_loss)

			# Summary and test
			summary, acc = sess.run([merged, accurancy], feed_dict = {x:im_test, y:label_test, drop:1})
			writer.add_summary(summary, epoch)
			
			if(epoch == start_epoch):
				min_loss = epcho_loss
			else:
				if(epcho_loss<=min_loss):
					saver.save(sess, 'model/traffic_sign')
					min_loss = epcho_loss
					print('saved model loss')
				if(acc>=max_pre):
					saver.save(sess, 'model1/traffic_sign')
					max_pre = acc
					print('saved model prediction')
				if(acc>=max_pre and epcho_loss<=min_loss):
					saver.save(sess, 'model2/traffic_sign')
					print('saved model both')

			print(acc)
			print('max: ', max_pre)
			save(min_loss, max_pre, epoch+1)

train_neural_network(x, y, drop, 4, isContinue = True)