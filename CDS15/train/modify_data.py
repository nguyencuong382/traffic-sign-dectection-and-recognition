import cv2
import numpy as np
import random
import pickle
import read_data


path = '../data'
img_size = 32
n_classes = 16


def show(src):
    window = 'image'
    cv2.namedWindow(window, cv2.WINDOW_AUTOSIZE)
    cv2.imshow(window, src)
    cv2.waitKey()
    cv2.destroyAllWindows()
def projection_transform(img, intensity):
    image_size = img.shape[1]
    d = image_size * 0.3 * intensity
    # for i in range(X.shape[0]):
    tl_top = np.random.uniform(-d, d)     # Top left corner, top margin
    tl_left = np.random.uniform(-d, d)    # Top left corner, left margin
    bl_bottom = np.random.uniform(-d, d)  # Bottom left corner, bottom margin
    bl_left = np.random.uniform(-d, d)    # Bottom left corner, left margin
    tr_top = np.random.uniform(-d, d)     # Top right corner, top margin
    tr_right = np.random.uniform(-d, d)   # Top right corner, right margin
    br_bottom = np.random.uniform(-d, d)  # Bottom right corner, bottom margin
    br_right = np.random.uniform(-d, d)   # Bottom right corner, right margin

    new_point = np.array((
            (tl_left, tl_top),
            (bl_left, image_size - bl_bottom),
            (image_size - br_right, image_size - br_bottom),
            (image_size - tr_right, tr_top)
        ), np.float32)
    old_point = np.array((
            (0, 0),
            (0, image_size),
            (image_size, image_size),
            (image_size, 0)
        ), np.float32)
    M = cv2.getPerspectiveTransform(new_point,old_point)
    dst = cv2.warpPerspective(img,M,(image_size,image_size), flags = cv2.INTER_LINEAR , borderMode=cv2.BORDER_REPLICATE)
    return dst
def eq_Hist(img):
    #Histogram Equalization
    img2=img.copy() 
    img2[:, :, 0] = cv2.equalizeHist(img[:, :, 0])
    img2[:, :, 1] = cv2.equalizeHist(img[:, :, 1])
    img2[:, :, 2] = cv2.equalizeHist(img[:, :, 2])
    return img2

def scale_img(img):
    img2=img.copy()
    sc_y=0.4*np.random.rand()+1.0
    img2=cv2.resize(img, None, fx=1, fy=sc_y, interpolation = cv2.INTER_CUBIC)
    c_x,c_y, sh = int(img2.shape[0]/2), int(img2.shape[1]/2), int(img_size/2)
    return img2

def crop(img, mar=0):
    c_x,c_y, sh = int(img.shape[0]/2), int(img.shape[1]/2), int(img_size/2-mar)
    return img[(c_x-sh):(c_x+sh),(c_y-sh):(c_y+sh)]

def rotate_img(img):
    c_x,c_y = int(img.shape[0]/2), int(img.shape[1]/2)
    ang = 30.0*np.random.rand()-15
    Mat = cv2.getRotationMatrix2D((c_x, c_y), ang, 1.0)
    return cv2.warpAffine(img, Mat, img.shape[:2], flags=cv2.INTER_LINEAR+cv2.WARP_FILL_OUTLIERS)

def sharpen_img(img):
    gb = cv2.GaussianBlur(img, (5,5), 20.0)
    return cv2.addWeighted(img, 2, gb, -1, 0)
#Compute linear image transformation ing*s+m
def lin_img(img,s=1.0,m=0.0):
    img2=cv2.multiply(img, np.array([s]))
    return cv2.add(img2, np.array([m]))

#Change image contrast; s>1 - increase
def contr_img(img, s=1.0):
    m=127.0*(1.0-s)
    return lin_img(img, s, m)

#just modify img
def transform_img(img):
    img2=sharpen_img(img)
    img2=crop(img2,3)
    img2=contr_img(img2, 1.5)
    img2=cv2.resize(img2, (img_size,img_size))
    return eq_Hist(img2)
    # return  img2

#modify and create new imgs
def augment_img(img):
    img=contr_img(img, 1.8*np.random.rand()+0.2)
    img=rotate_img(img)
    # img = projection_transform(img, 0.2)
    img=scale_img(img)
    img=cv2.resize(img, (img_size,img_size))
    return transform_img(img)

class DataSet(object):
    def __init__(self, imgs, labs):
        self._num_examples = imgs.shape[0]

        self._images = imgs
        self._labels = labs
        self._epochs_done = 0
        self._index_in_epoch = 0
        self._is_done = 0
    @property
    def labels(self):
        return self._labels

    @property
    def img_names(self):
        return self._img_names

    @property
    def num_examples(self):
        return self._num_examples

    @property
    def epochs_done(self):
        return self._epochs_done

    def next_batch(self, batch_size):
        """Return the next `batch_size` examples from this data set."""
        if(batch_size == self.num_examples):
            batch_size -= 1
        if(self._is_done):
            start = 0
            self._index_in_epoch = 0
            self._is_done = 0

        start = self._index_in_epoch
        self._index_in_epoch += batch_size

        if self._index_in_epoch > self._num_examples:
          # After each epoch we update this
          self._epochs_done += 1
          self._index_in_epoch = self._num_examples
          self._is_done = 1
          assert batch_size <= self._num_examples
        end = self._index_in_epoch

        return self._images[start:end], self._labels[start:end]


def data(path, img_size, n_classes, percent_train = 0.5, n_new_img=0, mix_data = False,
    load_from_file = False, save_newest_data = False):
    class DataSets(object):
        pass
    data_train_sets = DataSets()
    data_test_sets = DataSets()

    if(load_from_file):
        with open('data_train.p', 'rb') as f_train:
            data_train_sets = pickle.load(f_train)
        with open('data_test.p', 'rb') as f_test:
            data_test_sets = pickle.load(f_test)
        return data_train_sets, data_test_sets

    else:
        data_train, data_test = read_data.read_train(path, img_size, n_classes, percent_train)


        if(n_new_img > 0):
            new_data = []

            for data in data_train:
                img = data[1]
                lab = data[0]

                new_data.append([lab, transform_img(img)])

                for i in range(n_new_img-1):
                    new_img = augment_img(img)
                    new_data.append([lab, new_img])
                data_train = np.array(new_data, dtype = object)

        else:
            for i in range(len(data_train)):
                data_train[i][1] = transform_img(data_train[i][1])
            for i in range(len(data_test)):
                data_test[i][1] = transform_img(data_test[i][1])

        
        if(mix_data):
            new_index = [i for i in range(len(data_train))]
            random.shuffle(new_index)

            new_data = []
            for index in new_index:
                new_data.append(data_train[index])
            data_train = np.array(new_data, dtype = object)

        
        #data train
        imgs_train = []
        labs_train = []

        if(percent_train > 0):
            for d in data_train:
                imgs_train.append(d[1])
                id_ = d[0]
                lab = np.zeros(n_classes, 'int32')
                lab[id_ - 1] = 1
                labs_train.append(lab)

            imgs_train = np.array(imgs_train)
            labs_train = np.array(labs_train)
            data_train_sets = DataSet(imgs_train, labs_train)
        #data test
        imgs_test = []
        labs_test = []

        if(percent_train < 1):
            for d in data_test:
                imgs_test.append(d[1])
                id_ = d[0]
                lab = np.zeros(n_classes, 'int32')
                lab[id_ - 1] = 1
                labs_test.append(lab)

            imgs_test = np.array(imgs_test)
            labs_test = np.array(labs_test)
            data_test_sets = DataSet(imgs_test, labs_test)

        if(save_newest_data):
            if(np.any(imgs_train)):
                with open("data_train.p", "wb") as f:
                    pickle.dump(data_train_sets, f, pickle.HIGHEST_PROTOCOL)
                    print('saved newest data train with', percent_train)
            if(np.any(imgs_test)): 
                with open("data_test.p", "wb") as f:
                    pickle.dump(data_test_sets, f, pickle.HIGHEST_PROTOCOL)
                    print('saved newest data test with', 1-percent_train)

        return data_train_sets, data_test_sets

if __name__ == '__main__':
    data_train, data_test = data(path, img_size, n_classes, percent_train = 0.9, 
        n_new_img = 0, mix_data = False, load_from_file = False, save_newest_data = True)
    print('train: ', data_train.num_examples)
    print('test: ', data_test.num_examples)
    # imgs, labs = data_train.next_batch(10)
    # for d in zip(imgs, labs):
    #     print(d[1])
    #     show(d[0])
