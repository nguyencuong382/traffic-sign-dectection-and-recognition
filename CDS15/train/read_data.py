import os
import numpy as np
import cv2
import math

path = '../data'
img_size = 200
n_classes = 51

def show(src):
	window = 'image'
	cv2.namedWindow(window, cv2.WINDOW_AUTOSIZE)
	cv2.imshow(window, src)
	cv2.waitKey()
	cv2.destroyAllWindows()
def find_list_ings(dirFile, types = ['jpg', 'ppm', 'png', 'jpeg']):

	if(len(types) == 0):
		print("No types input to find...")
		return []

	list_files = os.listdir(dirFile)
	list_imgs = []

	for file in list_files:
		split = file.split('.')
		if(split[len(split) - 1] in types):
			list_imgs.append(file)

	return list_imgs

def preproce(path, img_size):
	print(path)
	try:
		img = cv2.imread(path, cv2.IMREAD_COLOR)
		resized = cv2.resize(img, (img_size, img_size),0,0, cv2.INTER_LINEAR)
		return resized
	except:
		print(path, 'is not image file')
		return []

def classify_path(path):
	s = path.split('.');
	return int(s[1])

def read_train(path, img_size, n_classes, percent_train = 0.5):
	data_train =[]
	data_test = []
	ids = os.listdir(path)
	#find ids' paths
	for id_ in ids:
		path_ob = path+"/"+id_
		list_imgs = find_list_ings(path_ob)

		#load data for each id
		n_imgs = len(list_imgs)
		n_imgs_train = int(np.ceil(n_imgs*percent_train))
		n_imgs_test = n_imgs - n_imgs_train
		
		#read for data train
		for i in range(n_imgs_train):
			path_img = path_ob + "/" + list_imgs[i]
			img = preproce(path_img, img_size)
			data_train.append([int(id_), img])

		#read for data test
		for i in range(n_imgs_train, n_imgs):
			path_img = path_ob + "/" + list_imgs[i]
			img = preproce(path_img, img_size)
			data_test.append([int(id_), img])
			
	return data_train, data_test
