import os
import numpy as np
import cv2
import time
import processbox

kernel = np.ones((3, 3), np.uint8)

# low_red = (75, 6, 94)
# low_red = (99, 18, 146)
low_red = (71,47,95) #1061
# low_red = (101, 67, 82)
# low_red = (104, 90, 129)
# low_red = (105, 32, 123) #1054
# low_red = (107, 87, 83)
# low_red = (83, 107, 96)
high= (255,255,255)

def show(img, name_window = 'window'):
	cv2.namedWindow(name_window, cv2.WINDOW_AUTOSIZE)
	cv2.imshow(name_window, img)

def find_traffic(img, low1, high):
	width, height, _ = img.shape

	hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

	# hsv = cv2.GaussianBlur(hsv, (5, 5), 0, 0)
	mask1 = cv2.inRange(hsv, low1, high)
	mask = mask1
	# mask = cv2.medianBlur(mask,5)
	mask = cv2.GaussianBlur(mask, (3, 3), 0, 0)
	mask = cv2.dilate(mask, kernel, iterations=1)

	im2, contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
	# print(contours)
	cv2.drawContours(mask, contours, -1, (255,255,255), thickness=cv2.FILLED)
	return mask

def contour(mask, img, min_area):
	im2, contours, hierarchy = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
	# print (len(contours))
	if (len(contours) > 0):
		boxes = []
		
		for c in contours:
			epsilon = 0.06 * cv2.arcLength(c, True)
			approx = cv2.approxPolyDP(c, epsilon, True)
			x, y, w, h = cv2.boundingRect(approx)
		   
			if(w*h > min_area):
				boxes.append([x, y, x+w, y+h])
		# for box in boxes:
		#     cv2.rectangle(mask, (box[0], box[1]), (box[2], box[3]), 255, 2)
		# show(mask)
		return processbox.group_boxes(boxes)
	else:
		return []
def find_cors(img, min_area):
	img2 = img.copy()
	mask = find_traffic(img, low_red, high)
	# cv2.imshow('mask', mask)
	return contour(mask, img2, min_area)

def take_path(path):
	paths = []
	list_imgs = os.listdir(path)
	for img in list_imgs:
		split_name = img.split('.')
		extension = split_name[len(split_name) - 1]
		if (extension == 'jpg'):
			paths.append(img)
	return paths

def find_test():
	dirFile = 'test_images/'
	paths = take_path(dirFile)
	print(paths)
	for path in paths:
		img = cv2.imread(dirFile + path, cv2.IMREAD_COLOR)
		# ret, blue = find_traffic(img, low_blue, high_blue, 'blue')
		img2= img.copy()
		mask= find_traffic(img, low_red, high)
		contour(mask, img,  min_area = 200)
		show(img2, 'img')
		show(mask, 'mask')
		k = cv2.waitKey() & 0xff
		if(k==27):
			break

if __name__ == '__main__':
	find_test()
