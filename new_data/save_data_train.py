import os
import xml.etree.ElementTree as ET
from time import gmtime, strftime
import time
import cv2

class Box:
	def __init__(self, bndbox, name, pose = 'Unspecified', truncated = 0, difficult = 0):
		self.name = name
		self.pose = pose
		self.truncated = truncated
		self.difficult = difficult
		self.bndbox = bndbox
	def getName(self):
		return self.name
	def getPose(self):
		return self.pose
	def getTruncated(self):
		return self.truncated
	def getDifficult(self):
		return self.difficult
	def getBndbox(self):
		return self.bndbox
	def setName(self, name):
		self.name = name
	def setBndbox(self, new_box):
		self.bndbox = new_box
def indent(elem, level=0):
	i = "\n" + level*"  "
	if len(elem):
		if not elem.text or not elem.text.strip():
			elem.text = i + "  "
		if not elem.tail or not elem.tail.strip():
			elem.tail = i
		for elem in elem:
			indent(elem, level+1)
		if not elem.tail or not elem.tail.strip():
			elem.tail = i
	else:
		if level and (not elem.tail or not elem.tail.strip()):
			elem.tail = i

def generate_xml(tree, new_objects, name_file, h ,w):
	root = tree.getroot()
	root[1].text = name_file
	root[4][0].text = str(w)
	root[4][1].text =str(h)
	objs = root.findall('object')
	for obj in objs:
		root.remove(obj)
	for obj in new_objects:
		new_obj = ET.Element('object')

		name = ET.Element('name')
		name.text = obj.getName()
		new_obj.append(name)

		pose = ET.Element('pose')
		pose.text = obj.getPose()
		new_obj.append(pose)

		truncated = ET.Element('truncated')
		truncated.text = str(obj.getTruncated())
		new_obj.append(truncated)

		difficult = ET.Element('difficult')
		difficult.text = str(obj.getDifficult())
		new_obj.append(difficult)

		box = obj.getBndbox()
		bndbox = ET.Element('bndbox')
		xmin = ET.Element('xmin')
		xmin.text = str(box[0])

		ymin = ET.Element('ymin')
		ymin.text = str(box[1])

		xmax = ET.Element('xmax')
		xmax.text = str(box[2])

		ymax = ET.Element('ymax')
		ymax.text = str(box[3])

		bndbox.append(xmin)
		bndbox.append(ymin)
		bndbox.append(xmax)
		bndbox.append(ymax)
		new_obj.append(bndbox)

		root.append(new_obj)
	indent(root)
	return tree

class Save:
	def __init__(self):
		path = os.path.dirname(os.path.abspath(__file__))

		self.tree = ET.parse(os.path.join(path, 'mau.xml'))
		self.path_classifier = os.path.join(path, 'classifier_data')
		self.path_detection = os.path.join(path, 'detection_data')

		if not os.path.exists(self.path_classifier):
			os.makedirs(self.path_classifier)
		if not os.path.exists(self.path_detection):
			os.makedirs(self.path_detection)
		self.num_imgs_classifier = 0
		self.num_imgs_detection = 0
		self.time_to_save = time.time()

	def check_path(self, name_objs):
		for name in name_objs:
			path = os.path.join(self.path_classifier, name)
			if not os.path.exists(path):
				os.makedirs(path)

	def save(self, img, boxes, name_objs, isSaveClassifier = False, isSaveDetection = False, min_area = 300):

		if( (time.time() - self.time_to_save) > 0.3):
			t = strftime("%Y%m%d%H%M%S", gmtime())

			if(isSaveClassifier):
				self.check_path(name_objs)

				for box, name in zip(boxes, name_objs):
					area = (box[2] - box[0])*(box[3] - box[1])

					if(area > min_area):
						sub_img = img[box[1]:box[3], box[0]:box[2]]

						patt_sub_img = os.path.join(self.path_classifier, 
													name, 
													'{}-{}.jpg' .format(t, self.num_imgs_classifier))
						
						cv2.imwrite(patt_sub_img, sub_img)
						self.num_imgs_classifier += 1
						print("Saved classifer: ", patt_sub_img)

						self.time_to_save = time.time()
			if(isSaveDetection and len(boxes) > 0):
				objs = []
				for box in boxes:
					area = (box[2] - box[0])*(box[3] - box[1])
					if(area > min_area):
						objs.append(Box(box, name_objs))
				if (len(objs) > 0):
					name_img = '{}-{}.jpg' .format(t, self.num_imgs_detection)
					name_xml = '{}-{}.xml' .format(t, self.num_imgs_detection)

					h, w = img.shape[0], img.shape[1]
					new_tree = generate_xml(self.tree, objs, name_img, h, w)
					new_tree.write(os.path.join(self.path_detection, name_xml))
					cv2.imwrite(os.path.join(self.path_detection, name_img), img)

					self.num_imgs_detection += 1
					print('Saved detection: ', name_img)

					self.time_to_save = time.time()


if __name__ == '__main__':
	saver = Save()
