import cv2
import numpy as np
import utils
def draw_mask(h = 480, w = 640):
	img = cv2.imread('./../rt/extracted/32.jpg')
	# points = [[0,0],[230,12], [240,122],[12,52]]	
	# img2 = utils.region(img, points)
	# # utils.show(img2)
	# wrap = utils.wrapperspective(img2, points)
	# wrap = np.hstack([img2, wrap])
	# utils.show(wrap)

	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	rat, thre = cv2.threshold(img, 120, 255, cv2.THRESH_BINARY)
	dis = utils.distance_trans(thre)
	dest = np.hstack([dis, thre])
	utils.show(dest)
	
if __name__ == '__main__':
	draw_mask()