import cv2
import numpy as np
def show(img):
	cv2.namedWindow('w', 1)
	cv2.imshow('w', img)
	cv2.waitKey()
	cv2.destroyAllWindows()


def points2cotour(points, isSubCnt = True):
	# points = [ [x,y], [x,y] ]
	# [ [[255 , 54]], [[338 , 62]] ,[[332 , 80]] ,[[292 , 69]] ]

	cnt = []

	for point in points:
		cnt.append([[point[0], point[1]]])

	if(isSubCnt):
		return np.array(cnt, dtype = np.int32)
	else:
		return np.array([cnt], dtype = np.int32)

def drawcnt(img, cnts):

	cv2.drawContours(img, cnts, -1, (255,255,255), thickness=cv2.FILLED)

	return img

def addWeight(img1, img2):
	return cv2.addWeighted(img1,0.7,img2,0.3,0)

def region(img, points):
	h, w, _ = img.shape

	mask = np.zeros((h,w), dtype = np.uint8)

	cv2.fillPoly(mask, np.array([points]), (255,255,255))
	return cv2.bitwise_and(img, img, mask = mask)

def wrapperspective(img, pts):
	pts = np.array(pts)
	rect = np.zeros((4, 2), dtype = "float32")

	s = pts.sum(axis = 1)
	rect[0] = pts[np.argmin(s)]
	rect[2] = pts[np.argmax(s)]

	diff = np.diff(pts, axis = 1)
	rect[1] = pts[np.argmin(diff)]
	rect[3] = pts[np.argmax(diff)]


	(tl, tr, br, bl) = rect

	widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
	widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))

	heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
	heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))


	maxWidth = max(int(widthA), int(widthB))
	maxHeight = max(int(heightA), int(heightB))

	dst = np.array([
					[0, 0],
					[maxWidth - 1, 0],
					[maxWidth - 1, maxHeight - 1],
					[0, maxHeight - 1]], dtype = "float32")
	M = cv2.getPerspectiveTransform(rect, dst)
	warp = cv2.warpPerspective(img, M, (maxWidth, maxHeight))

	return warp

def ero(img):
	kernel = np.ones((5,5), np.uint8)
	return cv2.erode(img, kernel, interations = 1)

def dil(img):
	kernel = np.ones((5,5), np.uint8)
	return cv2.dilation(img, kernel, interations = 1)

def mor_open(img):
	kernel = np.ones((5,5), np.uint8)
	return cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)

def distance_trans(thresh):
	kernel = np.ones((3,3),np.uint8)
	opening = cv2.morphologyEx(thresh,cv2.MORPH_OPEN,kernel, iterations = 1)
	# return opening
	# sure background area
	sure_bg = cv2.dilate(opening,kernel,iterations=3)
	# Finding sure foreground area
	dist_transform = cv2.distanceTransform(opening,cv2.DIST_L2,5)
	# return dist_transform
	ret, sure_fg = cv2.threshold(dist_transform,0.7*dist_transform.max(),255,0)
	# Finding unknown region
	# return sure_fg
	sure_fg = np.uint8(sure_fg)
	unknown = cv2.subtract(sure_bg,sure_fg)

	return unknown
