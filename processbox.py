import cv2
import color

def union(a,b):
	x1 = min(a[0], b[0])
	y1 = min(a[1], b[1])
	x2 = max(a[2], b[2])
	y2 = max(a[3], b[3])
	return [x1, y1, x2, y2]

def intersection(a,b, area_a, area_b, ratio_area = 0.4):
	x1 = max(a[0], b[0])

	y1 = max(a[1], b[1])
	x2 = min(a[2], b[2])
	y2 = min(a[3], b[3])
	w = x2 - x1
	h = y2 - y1
	# print(w, h)
	if(w > 0 and h >0):
		min_area = min(area_a, area_b)
		if(w*h/min_area > ratio_area):
			# print('yes')
			# cv2.waitKey()
			return True
	return False
	
def convert_boxes(boxes, h, w):
	new_boxes = []
	for box in boxes:
		ymin = int(box[0]*h)
		xmin = int(box[1]*w)
		ymax = int(box[2]*h)
		xmax = int(box[3]*w)

		new_boxes.append([xmin, ymin, xmax, ymax])
	return new_boxes
def find_valid_boxes(boxes, ratio_area):
	num_boxes = len(boxes)
	if(num_boxes > 1):
		areas = []
		for box in boxes:
			w = box[2] - box[0]
			h = box[3] - box[1]
			areas.append(w*h)

		
		index_false = [1 for i in range(num_boxes)]

		for i in range(num_boxes):
			if(index_false[i]):
				for j in range(i+1, num_boxes):
					if(index_false[j]):
						box_i = boxes[i]
						box_j = boxes[j]
						rat = intersection(box_i, box_j, areas[i], areas[j], ratio_area)

						if rat:
							if(areas[i] > areas[j]):
								index_false[j] = 0
							else:
								index_false[i] = 0
		new_boxes = []
		for i in range(num_boxes):
			if(index_false[i]):
				new_boxes.append(boxes[i])		
		return new_boxes

	else:
		return boxes

def find(boxes, score, h, w, min_score = 0.5, min_area = 10, ratio_area = 0.2):
	boxes = convert_boxes(boxes, h, w)
	areas = []
	boxes_valid = []

	for box, score in zip(boxes, score):
		if(score > min_score):
			w = box[2] - box[0]
			h = box[3] - box[1]
			if(h==0):
				h = 0.0001
			ratio = w/h
			if(0.3333 < ratio < 3):
				area = w * h
				if(area > min_area):
					areas.append(area)
					boxes_valid.append(box)
	# print(boxes_valid)
	# print(areas)
	return find_valid_boxes(boxes_valid, ratio_area)
	
def group_boxes(boxes):
	num_boxes = len(boxes)
	if(num_boxes > 1):
		areas = []
		for box in boxes:
			w = box[2] - box[0]
			h = box[3] - box[1]
			areas.append(w*h)

		index_false = [1 for i in range(num_boxes)]
		new_boxes = []
		for i in range(num_boxes - 1):
			if(index_false[i]):
				for j in range(i+1, num_boxes):
					if(index_false[j]):
						box_i = boxes[i]
						box_j = boxes[j]
						rat = intersection(box_i, box_j, areas[i], areas[j], 0)
						if not rat:
							top = 0
							low = 0
							if(box_i [1] > box_j[1]):
								top = j
								low = i
							else:
								top = j
								low = i
							x_top = boxes[top][2]
							y_top = boxes[top][3]

							x_low = boxes[low][2]
							y_low = boxes[low][1]

							# print('x', x_low  - x_top)
							# print('u', y_low - y_top)
							if(x_low  - x_top < 10):
								if(-1 < y_low - y_top < 40):
									new_boxes.append(union(box_i, box_j))
									# index_false[j] = 0
									# index_false[i] = 0
						else:
							if(areas[i] > areas[j]):
								index_false[j] = 0
							else:
								index_false[i] = 0

		for i in range(num_boxes):
			if(index_false[i]):
				new_boxes.append(boxes[i])
		return new_boxes
	else:
		return boxes

#if a apart of box in orginal box
def interest_boxes(boxes, orgin_box):

	w = orgin_box[2] - orgin_box[0]
	h = orgin_box[3] - orgin_box[1]
	areas_orgin = w*h

	new_boxes = []
	for box in boxes:
		w = box[2] - box[0]
		h = box[3] - box[1]

		rat = intersection(box, orgin_box, w*h, areas_orgin, 0.2)

		if rat:
			new_boxes.append(box)
	return new_boxes

def draw_box(img, boxes, scores):
	h, w = img.shape[0], img.shape[1]
	boxes = find(boxes[0], scores[0], h, w, min_score = 0.1, min_area = 100, ratio_area = 0.2)
	num_boxes = len(boxes)

	all_boxes = []
	if(num_boxes > 0):
		for box in boxes:
			xmin = box[0]
			ymin = box[1]
			xmax = box[2]
			ymax = box[3]
			area_box_orgin = (xmax - xmin) * (ymax - ymin)

			# img = cv2.rectangle(img,  (xmin, ymin), (xmax, ymax) , (255,255,255), 1)

			if(ymin > h/1000):
				mean_l = int(((xmax - xmin)+(ymax - ymin))/2)					

				if(area_box_orgin < 600):
					ymin -= 20
					xmin -= 20
				else:
					ymin -= 20
					xmin -= 20

				mean_l += 40
				xmax = xmin+mean_l
				ymax = ymin+mean_l
				
				if(xmin < 0):
					xmin = 0
				if(ymin < 0):
					ymin = 0

				sub_img = img[ymin:ymax, xmin:xmax]
				boxes_color = color.find_cors(sub_img, min_area = 10)

				for box_ in boxes_color:

					x1 = box_[0]+xmin
					y1 = box_[1]+ymin
					x2 = box_[2]+xmin
					y2 = box_[3]+ymin

					# if(self.num_frame < 200):
					# 	if(x1 < 0):
					# 		continue
					ratio = (x2 - x1)/(y2 - y1)
					if(0.6 < ratio < 1.6):
						if(y1 > h/2):
							new_area = (x2- x1) * (y2 - y1)
							if(new_area > 100):
								new_box = [x1, y1, x2, y2]
								if(intersection(new_box, box, new_area, area_box_orgin, 0.1)):
									all_boxes.append(new_box)
						else:
							new_box = [x1, y1, x2, y2]
							new_area = (x2- x1) * (y2 - y1)
							if(intersection(new_box, box, new_area, area_box_orgin, 0.1)):
								all_boxes.append(new_box)
					
		all_boxes = find_valid_boxes(all_boxes , 0.1)
	return all_boxes


if __name__ == '__main__':
	boxes = [[ 0.23203887,  0.63208663 , 0.26712185 , 0.65347826],
			[ 0.23148981 , 0.53406477 , 0.26013261 , 0.54874039],
			[ 0.22303659 , 0.5269593 ,  0.2484664  , 0.54205132],
			[ 0.         , 0.4303661  , 0.18588753 , 0.9028495 ],
			[ 0.01043404 , 0.06099863 , 0.02455596 , 0.09865893],
			[ 0.22313817 , 0.62760144 , 0.25003484 , 0.64740831],
			[ 0.0075351  , 0.0966472 ,  0.02083124 , 0.13105625],
			[ 0.00719983 , 0.12622365 , 0.02023454 , 0.16011703],
			[ 0.0168339  , 0.03212306 , 0.03271839 , 0.05388607],
			[ 0.         , 0.28122845 , 0.18889579 , 0.75765526]]

	score = [ 0.52332872 , 0.24497914,  0.09790866,  0.02983459 , 0.0297337 ,  0.02491187,
			0.02332942 , 0.01678457,  0.01625485 , 0.01570365]
	boxes = find(boxes, score, 960, 640, min_score = 0.2, min_area = 200)
	print(boxes)