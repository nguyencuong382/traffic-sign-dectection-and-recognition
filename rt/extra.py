import cv2
import os
from os.path import join, exists

def ext(dir_video):
	video = cv2.VideoCapture(dir_video)
	cv2.namedWindow("w", 1)
	num_frame = 0

	dir_file = 'extracted'
	if not exists(dir_file):
		os.makedirs(dir_file)

	while(1):
		rat, frame = video.read()
		if rat:
			num_frame += 1

			path_frame = join(dir_file, str(num_frame) + '.jpg')
			cv2.imwrite(path_frame, frame)
			print("Saved {}" .format(path_frame))

			cv2.imshow('w', frame)

			k = cv2.waitKey(30) & 0xff

			if(k==32):
				break
		else:
			break


if __name__ == '__main__':
	ext('test.avi')

