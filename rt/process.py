import cv2
from os.path import join, exists
def show(img):
	cv2.namedWindow('w', 1)
	cv2.imshow('w', img)
	cv2.waitKey()
	cv2.destroyAllWindows()

def read(num_frame):
	dirFile = 'extracted'
	path_frame = join(dirFile, str(num_frame) + '.jpg')
	if not exists(path_frame):
		print("No file: {}" .format(path_frame))
	else:
		print("opening frame...")

	return cv2.imread(path_frame)

if __name__ == '__main__':
	show(read(30))