import os
from os.path import join

dir_file = os.path.dirname(os.path.realpath(__file__))


class SaveBoxes:
	def __init__(self, labels_match):
		# .[id_frame, boxes, labels]
		self.data = []

		# ['dog', 'cat']
		# [1, 2]
		self.labels_match = labels_match
		self.name_out_file = join(dir_file, 'output.txt')

		self.label = self.name_traffic_sign = {
			'stop': 1,
			're trai': 2,
			're phai': 3,
			'cam re trai': 4,
			'cam re phai': 5,
			'cam di nguoc chieu': 6,
			'toc do toi da': 7,
			're phai 2': 8,
			're trai 2': 9,
			'to do toi da 30': 10,
			'het toc do toi da 30': 11,
			'x': 12	
		}


	def update_data(self, objs):
		#[id_frame, [boxes], [labels]]
		id_frame = objs[0]
		boxes = objs[1]
		labels = objs[2]

		for label, box in zip(labels, boxes):
			if label in self.labels_match:
				self.data.append([str(id_frame), str(self.label[label]), str(box[0])
					, str(box[1]), str(box[2]), str(box[3])])

	def save(self):
		with open(self.name_out_file, 'w') as f:
			f.write(str(len(self.data))+'\n')
			for obj in self.data:
				out_line = ' '.join(obj);
				print(out_line);
				f.write(out_line + '\n')
		print("Save result")


if __name__ == '__main__':

	save = SaveBoxes(['stop'])

	objs =[1, [[1,2,3,4], [1,2,3,4]], ['stop','cam re phai']]

	save.update_data(objs)
	objs =[2, [[1,2,2,4], [1,2,3,4]], ['stop','cam re trai']]
	save.update_data(objs)
	objs =[3, [[1,2,3,5], [1,2,3,4]], ['stop', 're phai']]
	save.update_data(objs)

	save.save()

