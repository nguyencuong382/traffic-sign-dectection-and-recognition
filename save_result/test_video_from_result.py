import cv2
import time

def load_result():
	objs = {}
	with open('output.txt', 'r') as f:
		data = f.read()
		data = data.split('\n')

		size = int(data[0])
		for i in range(1, size+1):
			tmp_data = data[i].split(' ')
			id_frame = int(tmp_data[0])
			id_label = int(tmp_data[1])
			x_min = int(tmp_data[2])
			y_min = int(tmp_data[3])
			x_max = int(tmp_data[4])
			y_max = int(tmp_data[5])

			objs[id_frame] = [id_label, [x_min, y_min, x_max, y_max]]
	return objs

def run_video(name_video):
	video = cv2.VideoCapture(name_video)
	cv2.namedWindow(name_video, cv2.WINDOW_AUTOSIZE)
	num_frame = 0
	total_time = 0
	objs = load_result()

	id_frames = objs.keys()

	while(1):
		rat, frame = video.read()
		if(rat):
			num_frame += 1
			frame = cv2.resize(frame, (640,480))

			t = time.time()
			if num_frame in id_frames:
				id_label = objs[num_frame][0]
				box = objs[num_frame][1]
				frame = cv2.rectangle(frame, (box[0] , box[1]), (box[2], box[3]) , (0,255,255), 2)

			
			if(num_frame > 1):
				total_time += time.time() - t

			cv2.imshow(name_video, frame)
			k = cv2.waitKey(5) & 0xff
			if(k == 32):
				cv2.waitKey()
			if(k==27):
				print("Average Time:", total_time/num_frame)
				break
		else:
			break


	video.release()
	cv2.destroyAllWindows()

if __name__ == '__main__':
	# load_result()
	# run_video('../test_video/00202.MTS')
	run_video('../test_video/testHV.mp4')