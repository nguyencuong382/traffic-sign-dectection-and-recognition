import os
import numpy as np
import cv2
kernel = np.ones((3,3), np.uint8)

high =  (255,255,255)

def show(img):
  window = 'window'
  cv2.namedWindow(window, cv2.WINDOW_AUTOSIZE)
  cv2.imshow(window, img)
  cv2.waitKey()
  cv2.destroyAllWindows()

def region_of_interest(img, vertices):
	mask = np.zeros_like(img)
	if len(img.shape) > 2:
		channel_count = img.shape[2]
		ignore_mask_color = (255,) * channel_count
	else:
		ignore_mask_color = 255

	cv2.fillPoly(mask, vertices, ignore_mask_color)
	masked_image = cv2.bitwise_and(img, mask)
	return masked_image

def find_traffic(image, low1, high):
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	gray = cv2.bilateralFilter(gray, 11, 17, 17)
	edged = cv2.Canny(gray, low1[0], low1[1])

	a, cnts, _ = cv2.findContours(edged.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
	cnts = sorted(cnts, key = cv2.contourArea, reverse = True)[:10]
	screenCnt = None

	for c in cnts:
	# approximate the contour
		peri = cv2.arcLength(c, True)
		approx = cv2.approxPolyDP(c, 0.02 * peri, True)
	 
		# if our approximated contour has four points, then
		# we can assume that we have found our screen
		if len(approx) == 6:
			screenCnt = approx
			break
	print(screenCnt)
	cv2.drawContours(image, [screenCnt], -1, (0, 255, 0), 3)


	return image


def get_track():
	x = cv2.getTrackbarPos('H_low', 'window')
	y = cv2.getTrackbarPos('S_low', 'window')
	z = cv2.getTrackbarPos('V_low', 'window')

	return [x, y, z]

frame = ''

def process(x):
	global frame
	low= get_track()
	mask = find_traffic(frame,low, high)
	cv2.imshow("window", mask);

def create_track():
	low = (107, 87, 83)
	cv2.createTrackbar('H_low', 'window',low[0], 255, process)
	cv2.createTrackbar('S_low', 'window', low[1], 255, process)
	cv2.createTrackbar('V_low', 'window', low[2], 255, process)

def find_video(path):
	video =  cv2.VideoCapture(path)
	cv2.namedWindow('window', cv2.WINDOW_AUTOSIZE)
	create_track()
	
	MODE = 'PAUSE'

	global frame
	rat, frame = video.read()
	if not rat:
		return
	while(1):
		if(MODE == 'PLAY'):
			rat, frame = video.read()
			frame = cv2.resize(frame, (640,480));
			if rat:
				process(1)
			else:
				break
		
		k = 0
		if(MODE == 'PLAY'):
			k = cv2.waitKey(30) & 0xff
		else:
			k = cv2.waitKey() & 0xff
		
		if(k == 27):
			break
		if(k == 32):
			if(MODE == 'PLAY'):
				MODE = 'PAUSE'
			else:
				MODE = 'PLAY'

def test_imgs():
	global frame
	frame = cv2.imread("Picture1.jpg", 1)
	cv2.namedWindow('window', cv2.WINDOW_AUTOSIZE)
	create_track()
	process(1)
	cv2.waitKey()

if __name__ == '__main__':
	# find_video('test_video/test111.mp4')
	# find_video('test_video/MVI_1049.avi')	
	# find_video('test_video/MVI_1061.avi')
	# find_video('test_video/MVI_1062.avi')
	# find_video('test_video/MVI_1063.avi')
	# find_video('test_video/MVI_1054.avi')
	find_video('test_video/00202.MTS')
	# test_imgs() 
	
