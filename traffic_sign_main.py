import tensorflow as tf
import numpy as np
import time
import cv2
import color
import processbox
from new_data.save_data_train import Save
from CDS15.final_pro.prediction import Classifier
from save_result.save_boxes import SaveBoxes
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'

class Object_Detection:
	def __init__(self):
		self.MODEL_NAME = 'traffic_sign_model'
		self.PATH_TO_CKPT = self.MODEL_NAME + '/frozen_inference_graph.pb'
		self.PATH_TO_LABELS = os.path.join('training', 'object-detection.pbtxt')
		self.NUM_CLASSES = 1
		self.num_sub_imgs = 0

		self.W_DES = 640
		self.H_DES = 480
		self.num_frame = 0
		
		self.saver = Save()
		self.classifier = Classifier()
		self.save_result = SaveBoxes(['cam re trai', 're phai', 'toc do toi da'])
		self.init_graph()

	def init_graph(self):
		self.detection_graph = tf.Graph()

		with self.detection_graph.as_default():
			od_graph_def = tf.GraphDef()
			with tf.gfile.GFile(self.PATH_TO_CKPT, 'rb') as fid:
				serialized_graph = fid.read()
				od_graph_def.ParseFromString(serialized_graph)
				tf.import_graph_def(od_graph_def, name='')

		with self.detection_graph.as_default():
			# Definite input and output Tensors for detection_graph
			self.image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
			# Each box represents a part of the image where a particular object was detected.
			self.detection_boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
			# Each score represent how level of confidence for each of the objects.
			# Score is shown on the result image, together with the class label.
			self.detection_scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
			self.detection_classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
			self.num_detections = self.detection_graph.get_tensor_by_name('num_detections:0')

	def load_test_imgs(self, path = 'test_images'):
		TEST_IMAGE_PATHS = []
		list_paths = os.listdir(path+'/')
		for name_file in list_paths:
			TEST_IMAGE_PATHS.append(os.path.join(path,name_file))
		return TEST_IMAGE_PATHS

	def show(self, img):
		window = 'window'
		cv2.namedWindow(window, cv2.WINDOW_AUTOSIZE)
		cv2.imshow(window, img)
		cv2.waitKey()
		cv2.destroyAllWindows()

	def convert_to_desire_size(self, img, objs):
		
		h, w = img.shape[0], img.shape[1]
		x_scale = self.W_DES / w
		y_scale = self.H_DES / h

		img = cv2.resize(img, (self.W_DES, self.H_DES))

		if(len(objs) > 0):
			img_org = img.copy()
			boxes_convert = []
			labels = []
			for label, box in objs.items():
				if label != 'x':
					xmin = int(box[0]*x_scale)
					ymin = int(box[1]*y_scale)
					xmax = int(box[2]*x_scale)
					ymax = int(box[3]*y_scale)

					boxes_convert.append([xmin, ymin, xmax, ymax])
					labels.append(label)
					img = cv2.rectangle(img, (xmin,ymin), (xmax, ymax) , (0,255,255), 2)


			# self.saver.save(img_org, boxes_convert, labels, isSaveClassifier = True, isSaveDetection = False)
			for name, box in zip(labels, boxes_convert):
				cv2.putText(img,name, (box[0],box[1]-10), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0,255,255))
			self.save_result.update_data([self.num_frame, boxes_convert, labels])
		return img

	def draw_box(self, img, boxes, scores):
		all_boxes = processbox.draw_box(img, boxes, scores)
		objs = {}
		objs_boxs = {}
		if(len(all_boxes) > 0):
			sub_imgs = []

			for box in all_boxes:
				sub_imgs.append(img[box[1]:box[3], box[0]:box[2]])
			names, predict_value = self.classifier.run(sub_imgs)

			
			for name in names:
				objs[name] = 0

			for name, box, value in zip(names, all_boxes, predict_value):
				if(objs[name] < value and value > 3):
					objs[name] = value
					objs_boxs[name] = box
						
		return self.convert_to_desire_size(img, objs_boxs)

	def run_video(self, name_video):
		video = cv2.VideoCapture(name_video)
		cv2.namedWindow(name_video, cv2.WINDOW_AUTOSIZE)

		with tf.Session(graph = self.detection_graph) as sess:
			total_time = 0
			while(1):
				rat, frame = video.read()
				if(rat):
					self.num_frame += 1
					self.frame = frame
					frame = cv2.resize(frame, (960,640))
					frame_expanded = np.expand_dims(frame, axis=0)

					t = time.time()
					(boxes, scores, classes, num) = sess.run([self.detection_boxes, self.detection_scores, self.detection_classes, self.num_detections],
						feed_dict={self.image_tensor: frame_expanded})
					
					frame = self.draw_box(frame, boxes, scores)
					if(self.num_frame > 1):
						total_time += time.time() - t

					cv2.imshow(name_video, frame)
					k = cv2.waitKey(5) & 0xff
					if(k == 32):
						cv2.waitKey()
					if(k==27):
						print("Average Time:", total_time/self.num_frame)
						self.save_result.save()
						break
				else:
					print("Average Time:", total_time/self.num_frame)
					self.save_result.save()
					break
			video.release()
			cv2.destroyAllWindows()

	def run_imgs(self):
		TEST_IMAGE_PATHS = self.load_test_imgs()
		with tf.Session(graph = self.detection_graph) as sess:
			for image_path in TEST_IMAGE_PATHS:
				t = time.time()
				image = cv2.imread(image_path, cv2.IMREAD_COLOR)


				image_np = cv2.resize(image, (640,480))				
				image_np_expanded = np.expand_dims(image_np, axis=0)				
				(boxes, scores, classes, num) = sess.run(
						[self.detection_boxes, self.detection_scores, self.detection_classes, self.num_detections],
						feed_dict={self.image_tensor: image_np_expanded})		
				
				image_np = self.draw_box(image_np, boxes, scores)
				
				# vis_util.visualize_boxes_and_labels_on_image_array(
				#     image_np,
				#     np.squeeze(boxes),
				#     np.squeeze(classes).astype(np.int32),
				#     np.squeeze(scores),
				#     self.category_index,
				#     use_normalized_coordinates=True,
				#     line_thickness=8)
				print(time.time()-t)
				self.show(image_np)

object_detec = Object_Detection()

object_detec.run_video('test_video/00202.MTS')

# object_detec.run_imgs()
